package main;

import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.EulerAngle;

public class Turret {
	String type;
	ArmorStand turret;
	int damage;
	int health;
	int attackSpeed;
	int range;
	int attackTimer = 0;

	Creature passenger;
	public int taskID = 0;
	
	public Turret(String name, Location location){
		type = name;
		turret = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
		CartelsAttackMode plugin =  Util.getPlugin();
		damage = plugin.getConfig().getInt("turrets."+type+".damage");
		health = plugin.getConfig().getInt("turrets."+type+".health");		
		attackSpeed = plugin.getConfig().getInt("turrets."+type+".attackSpeed");
		range = plugin.getConfig().getInt("turrets."+type+".range");
		//Set Turret Look
		setupTurret();
	}
	
	public void kill(String s){
		if(passenger != null && passenger.isDead() == false)
			passenger.remove();
		if(turret != null && turret.isDead() == false)
			turret.remove();
		Util.removeTurret(s, this);
	}
	
	public void target(LivingEntity target) {
		Guardian g = (Guardian)passenger;
		if(type.equals("Laser")){
			g.setTarget(target);
		}else if(type.equals("Mortar")){
			
		}
	}
	
	@SuppressWarnings("deprecation")
	private void setupTurret(){
		Location l = turret.getLocation();
        switch (type){
            case "Laser":
                passenger = l.getWorld().spawn(l, Guardian.class);
                turret.setPassenger(passenger);
                break;

            case "Fireball":
                passenger = l.getWorld().spawn(l, Blaze.class);
                turret.setPassenger(passenger);
                break;

            case "Mortar":
                passenger = l.getWorld().spawn(l, Zombie.class);
                turret.setPassenger(passenger);
                break;
            case "Ice":
                passenger = l.getWorld().spawn(l, Zombie.class);
                turret.setPassenger(passenger);
                break;
        }
        turret.setBasePlate(true);
        turret.setVisible(false);
        turret.setCustomName(ChatColor.translateAlternateColorCodes('&', type));
        turret.setCustomNameVisible(true);
        passenger.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 10));
        passenger.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 10));
        passenger.setRemoveWhenFarAway(false);
        String[] s = Util.getPlugin().getConfig().getString("turrets."+type+".block").split(":");
        turret.setHelmet(new ItemStack(Integer.parseInt(s[0]), 1, (short) Integer.parseInt(s[1])));
        // Runnable Tasks
        runTurretHeadFollow(Util.getPlugin());
	}
	
	@SuppressWarnings("deprecation")
	public void runTurretHeadFollow(CartelsAttackMode plugin){
		final HashMap<UUID, Location> fromLocation = new HashMap<UUID, Location>();

		this.taskID = Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				try{
					if (fromLocation.containsKey(turret.getUniqueId())) {
						if (fromLocation.get(turret.getUniqueId()).getChunk().isLoaded() &&
								!fromLocation.get(turret.getUniqueId()).equals(passenger.getEyeLocation())) {
							if (passenger.getTarget() != null) {
								double pitch = Math.toRadians(passenger.getEyeLocation().getPitch());
								double yaw = Math.toRadians(passenger.getEyeLocation().getYaw());
								EulerAngle a = new EulerAngle(pitch, yaw, 0);
								turret.setHeadPose(a);
							}
						}
					}else{
						if (turret.getUniqueId() != null && passenger != null) {
							fromLocation.put(turret.getUniqueId(), passenger.getEyeLocation());
						}
					}
				}catch(Exception x){}
			}
		}, 0L, 3L);
	}
	
	public int getAttackTimer() {
		return attackTimer;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}
}
