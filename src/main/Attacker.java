package main;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import net.citizensnpcs.api.trait.trait.Equipment;
import net.citizensnpcs.api.trait.trait.Equipment.EquipmentSlot;

public class Attacker {
	NPC npc;
	int health;
	String type;
	boolean targetBeacon;
	ItemStack[] gear = new ItemStack[5];
	String particle = null;
	int damage;
	int range = 0;
	int fireRate;
	int attackTimer;

	Entity targetEnt;
	UUID attackerUUID;
	
	@SuppressWarnings("deprecation")
	public Attacker(String name, Location location){
		location.setY(location.getY()+1);
		CartelsAttackMode plugin =  Util.getPlugin();
		type = plugin.getConfig().getString("attackers."+name+".type");
		attackerUUID = UUID.randomUUID();
		//Create NPC
		NPCRegistry registry = CitizensAPI.getNPCRegistry();
		NPC npc = registry.createNPC(EntityType.valueOf(plugin.getConfig().getString("attackers."+name+".mobType")), name);
		health = plugin.getConfig().getInt("attackers."+name+".health");
		targetBeacon = plugin.getConfig().getBoolean("attackers."+name+".targetBeacon");
		particle = plugin.getConfig().getString("attackers."+name+".particle");
		damage = plugin.getConfig().getInt("attackers."+name+".damage");
		range = plugin.getConfig().getInt("attackers."+name+".range");
		fireRate = plugin.getConfig().getInt("attackers."+name+".attackRate");
		attackTimer = fireRate;
		//Items
		try{
			Equipment ee = npc.getTrait(Equipment.class);
			//System.out.print("ee: "+ ee);
			ee.set(1, new ItemStack(plugin.getConfig().getInt("attackers."+name+".helmet")));
			ee.set(2, new ItemStack(plugin.getConfig().getInt("attackers."+name+".chestplate")));
			ee.set(3, new ItemStack(plugin.getConfig().getInt("attackers."+name+".leggings")));
			ee.set(4, new ItemStack(plugin.getConfig().getInt("attackers."+name+".boots")));
			ee.set(EquipmentSlot.HAND, new ItemStack(plugin.getConfig().getInt("attackers."+name+".hand")));
		}catch(Exception x){x.printStackTrace();}
		npc.spawn(location);
	}
	
	public String getParticle() {
		return particle;
	}
	
	public UUID getAttackerUUID() {
		return attackerUUID;
	}
	
	public NPC getNPC(){
		return this.npc;
	}
	
	public int getHealth() {
		return health;
	}
	
	public void setHealth(int health) {
		this.health = health;
	}
	
	public void setTarget(Entity e){
		targetEnt = e;
		if(targetEnt instanceof EnderCrystal){
			this.npc.getNavigator().setTarget(targetEnt.getLocation());
		}else
			this.npc.getNavigator().setTarget(targetEnt, true);
		//System.out.println("Target");
	}
	
	public Entity getTarget(){
		return targetEnt;
	}

	public void clearTarget(){
		this.npc.getNavigator().setTarget(this.npc.getEntity().getLocation());
		targetEnt = null;
	}

	public void stopWalk(){
		this.npc.getNavigator().setTarget(this.npc.getEntity().getLocation());
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public void setLookAt(Location l){
		this.npc.faceLocation(l);
	}
	
	public int getAttackTimer() {
		return attackTimer;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}
	
	public void remove(){
		try{this.npc.despawn();}catch(Exception x){}
		try{this.npc.destroy();}catch(Exception x){}
		health = 0;
	}
	
	public boolean isDead(){
		if(health <= 0)
			return true;
		return false;
	}
	
	public void kill(){
		((LivingEntity)this.npc.getEntity()).setHealth(0);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Util.getPlugin(), new Runnable(){
	        	public void run(){
	        		remove();
	        	}
		}, 25L);
	}

	public int getFireRate() {
		return fireRate;
	}

	public void setFireRate(int fireRate) {
		this.fireRate = fireRate;
	}
}
