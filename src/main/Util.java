package main;

import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import io.hotmail.com.jacob_vejvoda.Cartels.Cartels;

public class Util {

	public static CartelsAttackMode getPlugin(){
		return (CartelsAttackMode) Bukkit.getPluginManager().getPlugin("CartelsAttackMode");
	}
	
	public static Cartels getCartels(){
		return (Cartels) Bukkit.getPluginManager().getPlugin("Cartels");
	}
	
    public static void shootBeam(Attacker start, Entity target, final String effect, double speed){
		Location eyeLoc = start.getNPC().getEntity().getLocation();
		eyeLoc.setY(eyeLoc.getY()+1);
		double px = eyeLoc.getX();
		double py = eyeLoc.getY();
		double pz = eyeLoc.getZ();
		double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
		double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
		double x = Math.sin(pitch) * Math.cos(yaw);
		double y = Math.sin(pitch) * Math.sin(yaw);
		double z = Math.cos(pitch);
		double distance = start.getNPC().getEntity().getLocation().distance(target.getLocation());
		for (int i = 1 ; i <= getPlugin().scanRadius ; i++) {
			final Location loc = new Location(eyeLoc.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
			if(start.getNPC().getEntity().getLocation().distance(loc) <= distance){
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(getPlugin(), new Runnable() {
				     public void run() {
						displayParticle(effect, loc,0.3,0,5);
					 }
				}, (i));
			}else
				break;
		}
    }
    
    public static void displayParticle(String effect, Location l, double radius, int speed, int amount){
    	displayParticle(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, speed, amount);
    }
    
    private static void displayParticle(String effect, World w, double x, double y, double z, double radius, int speed, int amount){
		Location l = new Location(w, x, y, z);
		//System.out.println("V: " + getServer().getVersion());
		if(Bukkit.getServer().getVersion().contains("1.9")){
			if(radius == 0){
				ParticleEffects_1_8_8.sendToLocation(ParticleEffects_1_8_8.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_8_8.sendToLocation(ParticleEffects_1_8_8.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}
    }

    private static ArrayList<Location> getArea(Location l, double r, double t) {
        ArrayList<Location> ll = new ArrayList<Location>();
        for (double x = l.getX() - r; x < l.getX() + r; x = x + t)
            for (double y = l.getY() - r; y < l.getY() + r; y = y + t)
                for (double z = l.getZ() - r; z < l.getZ() + r; z = z + t)
                    ll.add(new Location(l.getWorld(), x, y, z));
        return ll;
    }
    
    public static void removeTurret(final String s, final Turret t){
    	Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(getPlugin(), new Runnable(){
    		public void run(){
    			ArrayList<Turret> ts = getPlugin().cartelTurrets.get(s);
    	    	ts.remove(t);
    	    	getPlugin().cartelTurrets.put(s, ts);
    		}
    	}, 3);
    }
}
