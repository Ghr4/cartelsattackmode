package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import io.hotmail.com.jacob_vejvoda.Cartels.Cartels;
import net.milkbowl.vault.economy.Economy;

public class CartelsAttackMode extends JavaPlugin implements Listener{
	File saveYML = new File(getDataFolder(), "save.yml");
	YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
	public Economy eco;
	ArrayList<Attack> currentAttacks = new ArrayList<Attack>();
	HashMap<String, ArrayList<Turret>> cartelTurrets = new HashMap<String, ArrayList<Turret>>();
	double scanRadius;
	
    @Override
    public void onEnable(){
		if(getServer().getPluginManager().getPlugin("Citizens") == null || getServer().getPluginManager().getPlugin("Citizens").isEnabled() == false) {
			getLogger().log(Level.SEVERE, "Citizens 2.0 not found or not enabled");
			getServer().getPluginManager().disablePlugin(this);	
			return;
		}	
    	getServer().getPluginManager().registerEvents(this, this); 
    	if (!new File(getDataFolder(), "config.yml").exists()) {
    		saveDefaultConfig();
    	}
      	//Register Saves
      	if (!saveYML.exists()) {
      		try {
      			saveYML.createNewFile();
      		} catch (IOException e) {
      			e.printStackTrace();
      		}
      	}
      	setupEconomy();
      	scanRadius = getConfig().getDouble("scanRadius");
      	turretScan();
      	scanTimer();
    }
    
    private boolean setupEconomy(){
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            eco = economyProvider.getProvider();
        }
        return (eco != null);
    }
    
    public void save(){
    	try {
    		saveFile.save(this.saveYML);
    	}catch (IOException Exception) {}
    }
    
    private void scanTimer(){
        // Start Attacker Manager Repeating Task
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){ public void run(){
        	for (Attack a : currentAttacks)
        		for(Attacker obj : a.attackers)
        			attackerScan(obj);
        }}, 5, (20));
    }
    
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		try{
			String n = p.getItemInHand().getItemMeta().getDisplayName();
			if(n.equals("§cEnd Attack")){
				Attack a = getAtack(p);
				a.endAttack();
				return;
			}else if(n.contains("§2§l") && n.contains(" Attacker")){
				Attack a = getAttack(p);
				if(a != null){
					//Spawn Attacker
					String t = n.replace("§2§l", "").replace(" Attacker", "");
					a.spawnAttacker(t);
					p.sendMessage("§4[§b*§4] §7"+t+" attacker spawned!");
					int am = p.getItemInHand().getAmount()-1;
					System.out.println("am: " + am);
					if(am > 0){
						p.getItemInHand().setAmount(am);
					}else
						p.setItemInHand(null);
					System.out.println("x");
				}
				return;
			}else if(n.contains("§2§l") && n.contains(" Turret")){
				int am = p.getItemInHand().getAmount()-1;
				if(am > 0){
					p.getItemInHand().setAmount(am);
				}else
					p.setItemInHand(null);
				System.out.println("Place Turret");
				String t = n.replace("§2§l", "").replace(" Turret", "");
				String cn = Util.getCartels().getConfig().getString("labs."+Util.getCartels().getPlayerUUID(p)+".name");
				ArrayList<Turret> tur = new ArrayList<Turret>();
				if(cartelTurrets.containsKey(cn))
					tur = cartelTurrets.get(cn);
				tur.add(new Turret(t, e.getClickedBlock().getLocation()));
				cartelTurrets.put(cn, tur);
				p.sendMessage("§4[§b*§4] §7"+t+" turret placed!");
			}
		}catch (Exception x) {}
    	Attack a = getAttack(p);
    	if(a != null && a.beaconHP > 0){
    		e.setCancelled(true);
    		p.sendMessage("§4[§b*§4] §cYou must destroy the beacon first!");
    	}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		Attack a = getAtack(p);
		if(a!= null)
			a.endAttack();
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent e){
    	Player p = e.getPlayer();
		if(cartelUnderAttack(p.getUniqueId().toString())){
			p.kickPlayer("Your cartel is under attack!");
		}
    }
    
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerClick(InventoryClickEvent e){
		String name = e.getInventory().getName();
		Player p = (Player)e.getWhoClicked();
		try{
			System.out.println("n: "+name);
			if (name.equals("§0§lAttack")){
				e.setCancelled(true);
				String n = e.getCurrentItem().getItemMeta().getDisplayName();
				if(n.equals("§eAttack")){
					p.closeInventory();
					attackCartel(p);
				}else if(n.equals("§eBuy Attackers")){
					p.closeInventory();
					openAttackersGUI(p);
				}else if(n.equals("§eBuy Turets")){
					p.closeInventory();
					openTurretsGUI(p);
				}
			}else if (name.contains("§0§lAttackers")){
				//§0§lAttackers
				//§0§lAttackers
				System.out.println("A: 1");
				e.setCancelled(true);
				String n = e.getCurrentItem().getItemMeta().getDisplayName();
				if(n.contains("§e") && n.contains(" Attacker")){
					System.out.println("A: 2");
					try{
						//Buy Attacker
						String t = n.replace("§e", "").replace(" Attacker", "");
						int cost = getConfig().getInt("attackers."+t+".cost");
						if(eco.getBalance(p) >= cost){
							eco.withdrawPlayer(p, cost);
							addAttacker(p,t,1);
							p.sendMessage("§4[§b*§4] §7You bought a "+t+" attacker for $"+cost);
						}else{
							p.sendMessage("§4[§b*§4] §cNot enough money!");
							p.closeInventory();
						}
					}catch(Exception x){x.printStackTrace();}
				}
			}else if (name.contains("§0§lTurrets")){
				e.setCancelled(true);
				String n = e.getCurrentItem().getItemMeta().getDisplayName();
				if(n.contains("§e") && n.contains(" Turret")){
					//Buy Attacker
					String t = n.replace("§e", "").replace(" Turret", "");
					int cost = getConfig().getInt("turrets."+t+".cost");
					if(eco.getBalance(p) >= cost){
						eco.withdrawPlayer(p, cost);
						addTurret(p,t,1);
						p.sendMessage("§4[§b*§4] §7You bought a "+t+" turret for $"+cost);
						//Add Turret Item
						p.getInventory().addItem(getItem(getConfig().getString("turrets."+t+".block"), "§c§l"+t+" Turret", 1, Arrays.asList("Right click to place", "Can only be placed once")));
					}else{
						p.sendMessage("§4[§b*§4] §cNot enough money!");
						p.closeInventory();
					}
				}
			}
		}catch(Exception x){}
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
    public void onTurretDeath(EntityDeathEvent e){
    	//System.out.println("E: " + e.getEntity().getType());
        if (e.getEntity().getType().equals(EntityType.GUARDIAN)){
            Guardian turretGuardian = ((Guardian)e.getEntity());
            Turret tur = getTurret(turretGuardian);
            //System.out.println("tur: " + tur);
            if (tur != null)
            	tur.kill(getCartel(tur));
        }
    }
    
    @EventHandler
    public void onTarget(EntityTargetEvent e){
    	if(e.getEntity().getType().equals(EntityType.GUARDIAN))
	    	if(e.getTarget() instanceof Player){
	            Guardian turretGuardian = ((Guardian)e.getEntity());
	            Turret tur =  getTurret(turretGuardian);
	            //System.out.println("tur: " + tur);
	            if (tur != null)
	            	e.setCancelled(true);
	    	}
    }
    
    private Turret getTurret(Guardian g){
    	Entity s = null;
    	for(Entity e : g.getNearbyEntities(2, 2, 2))
    		if(e.getPassenger().equals(g)){
    			s = e;
    			break;
    		}
    	if(s != null){
    		for (Map.Entry<String, ArrayList<Turret>> i : cartelTurrets.entrySet())
    			for(Turret t : i.getValue())
    				if(t.passenger.getUniqueId().equals(g.getUniqueId()))
    					return t;
    	}
    	return null;
    }
    
    private String getCartel(Turret tr){
    	for (Map.Entry<String, ArrayList<Turret>> i : cartelTurrets.entrySet())
    		for(Turret t : i.getValue())
    			if(t.passenger.getUniqueId().equals(tr.passenger.getUniqueId()))
    				return i.getKey();
    	return null;
    }
	
	private void attackCartel(Player p){
		try{
			if(getAttack(p) == null){
				String auuid = null;
				int i = 0;
				do{
					String id = getCartels().getRandomCartelUUID(p);
					if(!cartelUnderAttack(auuid))
						auuid = id;
					i =  + 1;
				}while(i < 999 && auuid == null);
				if(auuid != null){
					System.out.println("Start1: " + currentAttacks.size());
					Attack a = new Attack(p, auuid);
					System.out.println("a: " + a);
					System.out.println("c: " + currentAttacks);
					currentAttacks.add(a);
					System.out.println("Start2: " + currentAttacks.size());
				}else
					p.sendMessage("§4[§b*§4] §cAll the cartels are already under attack!");
			}else
				p.sendMessage("§4[§b*§4] §cYou are already attacking a cartel!");
		}catch(Exception x){x.printStackTrace();}
	}
	
	private Attack getAtack(Player p){
		System.out.println(currentAttacks.size());
		for(Attack a : currentAttacks){
			System.out.println(a.player);
			if(a.player.getName().equals(p.getName()))
				return a;
		}
		return null;
	}
	
	public void addAttacker(Player p, String t, int i){
		int amount = saveFile.getInt("attackers."+p.getUniqueId().toString()+"."+t)+i;
		saveFile.set("attackers."+p.getUniqueId().toString()+"."+t, amount);
		save();
	}
	
	public void addTurret(Player p, String t, int i){
//		int amount = saveFile.getInt("turrets."+p.getUniqueId().toString()+"."+t)+i;
//		saveFile.set("turrets."+p.getUniqueId().toString()+"."+t, amount);
//		save();
		ItemStack s = getItem(getConfig().getString("turrets."+t+".block"), "§2§l"+t+" Turret", i, Arrays.asList("Right-click to place"));
		p.getInventory().addItem(s);
	}
    
    private void openAttackGUI(Player p){
    	Inventory inv = getServer().createInventory(p, 9, "§0§lAttack");
    	inv.setItem(2, getItem(getConfig().getString("attackGUI.attack"), "§eAttack", 1, null));
    	inv.setItem(4, getItem(getConfig().getString("attackGUI.attackers"), "§eBuy Attackers", 1, null));
    	inv.setItem(6, getItem(getConfig().getString("attackGUI.turrets"), "§eBuy Turets", 1, null));
    	p.openInventory(inv);
    }
    
    private void openAttackersGUI(Player p){
    	Inventory inv = getServer().createInventory(p, 9, "§0§lAttackers");
    	for (String n : getConfig().getConfigurationSection("attackers").getKeys(false))
    		inv.addItem(getItem(getConfig().getString("attackers."+n+".icon"), "§e"+n+" Attacker", 1, null));
    	p.openInventory(inv);
    }
    
    private void openTurretsGUI(Player p){
    	Inventory inv = getServer().createInventory(p, 9, "§0§lTurrets");
    	for (String n : getConfig().getConfigurationSection("turrets").getKeys(false))
    		inv.addItem(getItem(getConfig().getString("turrets."+n+".icon"), "§e"+n+" Turret", 1, null));
    	p.openInventory(inv);
    }
    
	@SuppressWarnings("deprecation")
	public ItemStack getItem(String setItemString, String name, int amount, List<String> loreList){
		ItemStack item = null;
		if (setItemString.contains(":")){
			String[] split = setItemString.split(":");
			int setItem = Integer.parseInt(split[0]);
			int subtype = Integer.parseInt(split[1]);
			item = new ItemStack(setItem, amount, (short)subtype);
		}else{
			int setItem = Integer.parseInt(setItemString);
			item = new ItemStack(setItem, amount);
		}
		ItemMeta m = item.getItemMeta();
		if(name != null)
			m.setDisplayName(name.replace("&", "§"));
		if(loreList != null){
			List<String> nll = new ArrayList<String>();
			for(String s : loreList) 
				nll.add(s.replace("&", "§"));
			m.setLore(nll);
		}
		m.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(m);
		return item;
	} 
	
    public Cartels getCartels(){
    	return (Cartels) Bukkit.getServer().getPluginManager().getPlugin("Cartels");
    }
    
	public Attack getAttack(Player p){
		for(Attack a : currentAttacks)
			if(a.player.equals(p))
				return a;
		return null;
	}
	
	public Attack getAttack(Chicken b){
		for(Attack a : currentAttacks)
			if(a.beacon.equals(b))
				return a;
		return null;
	}
	
	public boolean cartelUnderAttack(String uuid){
		for(Attack a : currentAttacks)
			if(a.cartelUUID.equals(uuid))
				return true;
		return false;
	}
	
	public void deleteGame(final Attack a){
	    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
	        public void run(){
	    		currentAttacks.remove(a);
	        }
	    }, 4L);
	}
	
    public void attackerScan(Attacker obj){
    	System.out.println("Scan");
        //final int range = DataManager.AttackerScanRange;
    	final int range = 35;
     	if(obj != null){
            	//Time
            	obj.setAttackTimer(obj.getAttackTimer() - 5);
            	//Death?
            	if(obj.getHealth() <= 0){
            		obj.kill();
            		//LivingEntity le = obj.getNPC().getEntity().getWorld().spawnEntity(obj.getNPC().getEntity(), arg1)
            	}else if(obj.getAttackTimer() <= 0){
            		obj.setAttackTimer(obj.getFireRate());
                	//Attack
                    if(obj.getTarget() == null){
                    	//System.out.println("No target");
                        LivingEntity turret = null;
                        Chicken beacon = null;
                        double distance = scanRadius;

                        if(obj.getNPC() != null && obj.getNPC().getEntity() != null){
                            for(Entity e : obj.getNPC().getEntity().getNearbyEntities(range, range, range)){
                            	System.out.println("Found: " + e.getType());
                            	//System.out.println("Pas: " + e.getPassenger());
                                if(e.getType().equals(EntityType.ARMOR_STAND) && e.getPassenger() != null && (e.getPassenger().getType().equals(EntityType.GUARDIAN))){
                                    LivingEntity gu = (LivingEntity) e.getPassenger();
                                    double far = obj.getNPC().getEntity().getLocation().distance(gu.getLocation());
                                    if(far < distance){
                                        distance = obj.getNPC().getEntity().getLocation().distance(gu.getLocation());
                                        turret = gu;
                                    }
                                }else if(e.getType().equals(EntityType.CHICKEN))
                                    beacon = (Chicken) e;
                            }
                        }

                        if(turret != null){
                            //Target Turret
                            obj.setTarget(turret);
                        }else if(beacon != null){
                        	obj.setTarget(beacon);
                            //obj.setTarget(beacon);
                        }
                    }else{
                        if(obj.getTarget().isDead()){
                            obj.clearTarget();
                        }else if(obj.type.equals("ranged")){
                            double distance = obj.getNPC().getEntity().getLocation().distance(obj.getTarget().getLocation());
                            if(distance < obj.range){
                                //Attack
                            	obj.stopWalk();
                            	obj.setLookAt(obj.getTarget().getLocation());
                                Util.shootBeam(obj, obj.getTarget(), obj.getParticle(), obj.getFireRate());
                        		if(obj.getTarget() instanceof Chicken){
                        			Attack a = getAttack((Chicken)obj.getTarget());
                        			if(a != null){
                        				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                        			}
                        		}else
                        			((Damageable)obj.getTarget()).damage(obj.getDamage());
                            }
                        }else if(obj.type.equals("melee")){
                            //Attack Melee
                    		if(obj.getTarget() instanceof Chicken){
                    			Attack a = getAttack((Chicken)obj.getTarget());
                    			if(a != null){
                    				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                    			}
                    		}
                        }else if(obj.type.equals("explode")){
                            double distance = obj.getNPC().getEntity().getLocation().distance(obj.getTarget().getLocation());
                            if(distance <= 1){
                                //Blow Up
                                Location l = obj.getNPC().getEntity().getLocation();
                                Util.displayParticle("BIG_EXPLODE", l,0,1,1);
                                l.getWorld().playSound(l, Sound.EXPLODE, 1, 1);
                        		if(obj.getTarget() instanceof Chicken){
                        			Attack a = getAttack((Chicken)obj.getTarget());
                        			if(a != null){
                        				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                        			}
                        		}else
                                	((Damageable)obj.getTarget()).damage(obj.getDamage());
                            }
                        }
                    }
            	}
     	}
    }
    
    public void turretScan(){
        // Start Turret Manager Repeating Task
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Util.getPlugin(), new Runnable(){ public void run(){
        	for (Map.Entry<String,  ArrayList<Turret>> i : Util.getPlugin().cartelTurrets.entrySet())
    	        for (Turret obj : i.getValue()) {
        			 if(obj != null && obj.turret != null && obj.passenger != null){
        				 obj.setAttackTimer(obj.attackTimer - 5);
        				 if(obj.getAttackTimer() <= 0){
        					 System.out.println("Speed: " + obj.attackSpeed);
        					 obj.setAttackTimer(obj.attackSpeed);
        					 int range = obj.range;
		        			 for(Entity e : obj.turret.getNearbyEntities(range, range, range)){
		        				 Attacker ao = getAttacker(e);
		        				 //System.out.println("AO: " + ao);
		        				 if(ao != null && ao.getNPC().getEntity().getLocation().distance(obj.passenger.getLocation()) > 2){
		        					 obj.target((LivingEntity)ao.getNPC().getEntity());
		        					 ao.setHealth(ao.getHealth()-obj.damage);
		        					 System.out.println("HP: " + ao.getHealth());
		        					 break;
		        				 }
		        			 }
        				 }
        			 }
	        	 }
        }}, 5, (20));
    }
    
    @SuppressWarnings("deprecation")
	public Attacker getAttacker(Entity e){
    	for(Attack a : currentAttacks)
    		for(Attacker at : a.attackers)
    			if(at.npc.getBukkitEntity().getUniqueId().equals(e.getUniqueId()))
    				return at;
    	return null;
    }
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	if((cmd.getName().equals("attack")) || (cmd.getName().equals("att"))){
    		try{
	    		if(args != null && args.length > 0 && args[0].equals("reload")){
	    			reloadConfig();
	    			scanRadius = getConfig().getDouble("scanRadius");
	    			sender.sendMessage("§eReloaded config!");
	    			return true; 
	    		}else if(sender instanceof Player){
	    			Player p = (Player)sender;
	    			openAttackGUI(p);
	    			return true;
	    		}
    		}catch(Exception e){}
			sender.sendMessage("§cWrong command!");
			sender.sendMessage("§e/attack reload");
			sender.sendMessage("§e/attack");
    	}
    	return true; 
    }
}
