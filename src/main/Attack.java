package main;

import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import net.milkbowl.vault.economy.Economy;

public class Attack {
	public Player player;
	public String cartelUUID;
	public Location attackSpawn = null;
	PreviousStuff pStuff;
	public LivingEntity beacon;
	public int beaconHP;
	public ArrayList<Attacker> attackers = new ArrayList<Attacker>();
	CartelsAttackMode plugin;

	public Attack(Player p, String auuid) {
		player = p;
		plugin =  Util.getPlugin();
		cartelUUID = auuid;
		//Get Spawn
		attackSpawn = plugin.getCartels().getSpawn(auuid);
		attackSpawn.setX(attackSpawn.getX()+plugin.getConfig().getDouble("attackSpawn.x"));
		attackSpawn.setY(attackSpawn.getY()+plugin.getConfig().getDouble("attackSpawn.y"));
		attackSpawn.setZ(attackSpawn.getZ()+plugin.getConfig().getDouble("attackSpawn.z"));
		pStuff = new PreviousStuff(p);
		int base = plugin.getConfig().getInt("beaconHP");
		int lvl = plugin.getCartels().getCarteLevel(cartelUUID);
		int ap = plugin.getConfig().getInt("beaconHPLevelPercent");
		beaconHP = (int)(base+(((base/100.0)*ap)*lvl));
		startAttack();
	}
	
	@SuppressWarnings("deprecation")
	private void startAttack(){
		player.sendMessage("§4[§b*§4] §7You have started the attack!");
		player.getInventory().clear();
		player.teleport(attackSpawn);
		//Get Beacon
//		int r = 35;
//		for(Entity e : player.getNearbyEntities(r, r, r))
//			if(e instanceof Chicken){
//				beacon = (LivingEntity) e;
//				break;
//			}
		Location bLoc = Util.getCartels().getBeaconSpawn(cartelUUID);
		beacon = (LivingEntity) player.getWorld().spawnEntity(bLoc, EntityType.CHICKEN);
		beacon.setMaxHealth(plugin.getConfig().getInt("beaconHP"));
		beacon.setHealth(plugin.getConfig().getInt("beaconHP"));
		beacon.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (999*999), 0));
		beacon.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (999*999), 999));
		//Give Leve Item
		player.getInventory().setItem(8, Util.getPlugin().getItem(Material.BARRIER.getId()+"", "§cEnd Attack", 1, null));
		//Give Attackers
		try{
			for (String at : plugin.saveFile.getConfigurationSection("attackers."+player.getUniqueId().toString()).getKeys(false)){
				int i = plugin.saveFile.getInt("attackers."+player.getUniqueId().toString()+"."+at);
				if(i > 0){
					ItemStack s = plugin.getItem(plugin.getConfig().getString("attackers."+at+".egg"), "§2§l"+at+" Attacker", i, Arrays.asList("Right click to place", "Will start at attack spawn"));
					player.getInventory().addItem(s);
				}
			}
			plugin.saveFile.set("attackers."+player.getUniqueId().toString(), null);
			plugin.save();
		}catch(Exception x){}
	}
	
	public void endAttack(){
		//Save Unused Attackers
		System.out.println("end");
		for(ItemStack s : player.getInventory())
			if(s != null)
				try{
					String in = s.getItemMeta().getDisplayName();
		    		if(in.contains("§2§l") && in.contains(" Attacker")){
		    			String t = in.replace("§2§l", "").replace(" Attacker", "");
		    			plugin.addAttacker(player,t,s.getAmount());
		    		}
				}catch(Exception x){}
		pStuff.restore();
		if(attackers != null && attackers.isEmpty() == false)
			for(Attacker obj : attackers)
				obj.kill();
		player.sendMessage("§4[§b*§4] §7Attack completed!");
		//Restore Beacon
		//if(beaconHP <= 0 && beacon.isDead())
		//	plugin.generateBeacon(cartelUUID);
		//Remove Attack
		plugin.deleteGame(this);
	}
	
	@SuppressWarnings("deprecation")
	public void setBeaconHP(int i){
		System.out.println("beaconHP: " + beaconHP);
		System.out.println("Set: " + i);
		beaconHP = i;
		if(beaconHP <= 0){
            Location l = beacon.getLocation();
            Util.displayParticle("BIG_EXPLODE", l,0,1,1);
            l.getWorld().playSound(l, Sound.EXPLODE, 1, 1);
            beacon.remove();
            player.sendMessage("§4[§b*§4] §7You have won the battle, you have 1 minute to loot chests!");
            //Money
            Economy eco = plugin.eco;
            int per = plugin.getConfig().getInt("winMoneyPercent");
            double cMoney = eco.getBalance(plugin.getCartels().getOwnerFromUUID(cartelUUID));
            int m = (int)((cMoney/100.0)*per);
            eco.depositPlayer(player.getName(), m);
            endTimer();
		}
	}
	
	private void endTimer(){
	    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
	        public void run(){
	        	endAttack();
	        }
	    }, 60 * 20L);
	}
	
    public void spawnAttacker(String t){
    	Attacker a = new Attacker(t, attackSpawn);
    	attackers.add(a);
    }
}

class PreviousStuff {
	public Player player;
    public Location getLocation;
    public ItemStack[] getInventory;
    public ItemStack[] getArmour;
    public double getHealth;
    public int getFood;
    public float getXP;
    public int getLevel;
    public GameMode getGameMode;
 
    PreviousStuff(Player p){
    	getLocation = p.getLocation();
		getInventory = p.getInventory().getContents();
		getArmour = p.getInventory().getArmorContents();
		getHealth = p.getHealth();
		getFood = p.getFoodLevel();
		getXP = p.getExp();
		getGameMode = p.getGameMode();
		getLevel = p.getLevel();
		player = p;
    }
    
    public void restore(){
		try{
			Location preLock = getLocation;
			int preFood = getFood;
			double preHealth = getHealth;
			float preXP = getXP;
			int preLvl = getLevel;
			ItemStack[] preInv = getInventory;
			ItemStack[] preAmour = getArmour;
			player.setFallDistance(0);
			player.teleport(preLock);
			player.setVelocity(new Vector(0, 0, 0));
			player.setFoodLevel(preFood);
			player.setFireTicks(0);
			player.setLevel(preLvl);
			player.setExp(preXP);
			player.setHealth(preHealth);
			//player.getInventory().clear();
			player.getInventory().setContents(preInv);
			player.getInventory().setArmorContents(preAmour);
			player.updateInventory();
			player.setGameMode(getGameMode);
		}catch(Exception e){}
    }
}
